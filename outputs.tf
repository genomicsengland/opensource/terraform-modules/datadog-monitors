output "monitor_ids" {
  value = tomap({
    for key, value in datadog_monitor.monitor : key => value.id
  })
}
