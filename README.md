This is the module that creates Datadog monitors

Example usage:
Since version 2022.11.16 the module has been updated to use implicit provider passing, this requires the provider to be declared in the root module.

```
provider "aws" {
  region = var.region
  alias  = "core_shared"
  assume_role {
    role_arn = "arn:aws:iam::512426816668:role/ReadDatadogKeys"
  }
}

provider "datadog" {
  api_url = "https://api.datadoghq.eu/"
  api_key = data.aws_secretsmanager_secret_version.datadog_api_key.secret_string
  app_key = data.aws_secretsmanager_secret_version.datadog_app_key.secret_string
}

data "aws_secretsmanager_secret_version" "datadog_api_key" {
  secret_id = "arn:aws:secretsmanager:eu-west-2:512426816668:secret:/prod/root/datadog/api_key-1tsRdO"
  provider  = aws.core_shared
}

data "aws_secretsmanager_secret_version" "datadog_app_key" {
  secret_id = "arn:aws:secretsmanager:eu-west-2:512426816668:secret:/prod/root/datadog/app_key-PtwBEP"
  provider  = aws.core_shared
}

module "datadog-monitors" {
  source = <path to this repository, inculding desired tag>
  region = var.region

  datadog_monitors = local.Datadog_monitors
}
```

`datadog_monitors (map(datadog_monitor))` - map of `datadog_monitor` objects.

`datadog_monitor map(map(any))`:

```
datadog_monitor = {
  parameters          = map(any)
  tags                = map(any)
  custom_tags         = map(any)
  threshold_windows   = map(any)
  thresholds          = map(any)
  renotify_statuses   = map(any)
  notify_by           = map(any)
}
```
```
parameters = {
  name                     = string
  type                     = string
  query                    = string   
  message                  = string
  escalation_message       = string
  require_full_window      = bool 
  notify_no_data           = bool
  new_group_delay          = number
  evaluation_delay         = number
  no_data_timeframe        = number
  renotify_interval        = number
  notify_audit             = bool
  timeout_h                = number
  include_tags             = bool
  enable_logs_sample       = bool
  force_delete             = bool
  priority                 = number
  on_missing_data          = string
  notification_preset_name = string
  group_retention_duration = string
  }
```
```
monitor_thresholds = {
  warning           = string
  warning_recovery  = string
  critical          = string
  critical_recovery = string
  ok                = string
  unknown           = string
}
```
```
monitor_threshold_windows {
  recovery_window = string
  trigger_window  = string
}
```

`parameters`

| Name | Description | Type | Required | Note |
|------|-------------|------|----------|:----:|
| name | Name of Datadog monitor | `string` | yes |
| type | The type of the monitor. Valid values: <ul><li>`composite`</li><li>`event alert`</li><li> `log alert`</li><li> `metric alert`</li><li> `process alert`</li><li> `query alert`</li><li> `rum alert`</li><li> `service check`</li><li> `synthetics alert`</li><li> `trace-analytics alert`</li><li> `slo alert`</li><li> `event-v2 alert`</li><li> `audit alert` | `string` | yes | The monitor type cannot be changed after a monitor is created |
| query | The monitor query to notify on | `string`| yes | This is not the same query you see in the UI and the syntax is different depending on the monitor type, please see the [API Reference](https://docs.datadoghq.com/api/latest/monitors/#create-a-monitor) for details. `terraform plan` will validate query contents unless validate is set to false |
| message | A message to include with notifications for this monitor | `string` | yes |
| escalation_message | A message to include with a re-notification. Supports the @username notification allowed elsewhere. | `string` | no |
| require_full_window | Whether this monitor needs a full window of data before it's evaluated | `bool` | no |
| notify_no_data | Whether this monitor will notify when data stops reporting. Defaults to `false`. | `bool` | no |
| new_group_delay | Time (in seconds) to skip evaluations for new groups | `number` | no |
| evaluation_delay | Time (in seconds) to delay evaluation, as a non-negative integer. Only applies to metric alert | `number` | no |
| no_data_timeframe | The number of minutes before a monitor will notify when data stops reporting. Provider defaults to 10 minutes | `number` | no |
| renotify_interval | The number of minutes after the last notification before a monitor will re-notify on the current status. It will only re-notify if it's not resolved | `number` | no |
| notify_audit | Whether tagged users will be notified on changes to this monitor. Defaults to `false`. | `bool` | no |
| timeout_h | The number of hours of the monitor not reporting data before it will automatically resolve from a triggered state | `number` | no |
| include_tags | Whether notifications from this monitor automatically insert its triggering tags into the title. Defaults to `true` | `bool` | no |
| enable_logs_sample | Whether or not to include a list of log values which triggered the alert. This is only used by log monitors. Defaults to `false`. | `bool` | no |
| force_delete | Whether this monitor can be deleted even if it’s referenced by other resources (e.g. SLO, composite monitor) | `bool` | no |
| priority | DataDog priority. From 1 (critical) to 5 (info) | `number` | no |
| on_missing_data | Controls how groups or monitors are treated if an evaluation does not return any data points. Valid values are: show_no_data, show_and_notify_no_data, resolve, and default. | `string` | no |
| notification_preset_name | Controls how detailed the alert message will be. Valid values are: show_all, hide_query, hide_handles, and hide_all. | `string` | no |
| group_retention_duration | The time span after which groups with missing data are dropped from the monitor state. | `string` | no |

`thresholds`

| Name | Description | Type | Required |
|------|-------------|------|:--------:|
| critical | The monitor CRITICAL threshold. Must be a number. | `string` | no |
| critical_recovery | The monitor CRITICAL recovery threshold. Must be a number | `string` | no |
| ok | The monitor OK threshold. Must be a number | `string` | no |
| unknown | The monitor UNKNOWN threshold. Must be a number | `string` | no |
| warning | The monitor WARNING threshold. Must be a number | `string` | no |
| warning_recovery | The monitor WARNING recovery threshold | `string` | no |

For more details please check [Nested Schema for monitor_thresholds](https://registry.terraform.io/providers/DataDog/datadog/latest/docs/resources/monitor#nested-schema-for-monitor_thresholds)

`threshold_windows`

| Name | Description | Type | Required |
|------|-------------|------|:--------:|
| recovery_window | Describes how long an anomalous metric must be normal before the alert recovers | `string` | no |
| trigger_window | Describes how long a metric must be anomalous before an alert triggers | `string` | no |

For more details please check [Nested Schema for monitor_threshold_windows](https://registry.terraform.io/providers/DataDog/datadog/latest/docs/resources/monitor#nested-schema-for-monitor_threshold_windows)

`datadog_monitors` example:
```
locals {
  Datadog_monitors = {
    unhealthy_workspaces = {
      parameters = {
        type    = "metric alert"
        name    = "At least one unhealthy WorkSpace in Service Management VDI space"
        message = "@slack-alerts-iaas"

        query = format("sum(last_5m):sum:aws.workspaces.unhealthy{account_id:%s} by {account_id} != 0", var.account_id)

        notify_no_data    = true
        evaluation_delay  = 300
        no_data_timeframe = 30
      },
      tags = var.default_tags
    },
    unhealthy_workspaces_service_desk = {
      parameters = {
        type    = "metric alert"
        name    = "cvs-VDI P3-At least one unhealthy WorkSpace in Service Management VDI space"
        message = "@ge-servicedesk@genomicsengland.co.uk"

        query = format("sum(last_5m):sum:aws.workspaces.unhealthy{account_id:%s} by {account_id} != 0", var.account_id)

        evaluation_delay = 300
      },
    },
    automation_lambda_errors = {
      parameters = {
        type    = "metric alert"
        name    = "WorkSpaces automation lambda errors"
        message = "@opsgenie-Cloud_Enablement"

        query = format("sum(last_5m):sum:aws.lambda.errors{account_id:%s,functionname:workspacesautomation} != 0", var.account_id)

        notify_no_data    = true
        evaluation_delay  = 300
        no_data_timeframe = 30
      },
      tags = var.default_tags
    }
  }
}
```
Prerequisites:
AWS account is [integrated with Datadog](https://gitlab.com/genomicsengland/opensource/terraform-modules/datadog-integration)

Limitations:
This module is intended to be used within your GitLab CI/CD pipeline using
GEL standardized [GitLab runners](https://gitlab.com/genomicsengland/cloud/aws-core/shared/gitlab-runner)

